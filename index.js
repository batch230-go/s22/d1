// console.log("Hello World!");

// Javascript has built-in functions and methods for arrays. This allows to manipulate and access array items
// Mutator methods
/*
    - Mutator methods are functions that mutate or change an array after they're created
    - These mutator methods manipulate the original array performing various tasks such as adding and removing elements
*/

let fruits = ["Apple", "Orange", "Chico", "Lemon"];

// push()
/*
        - Adds an element in the end of an array AND returns the array's length
        - Syntax
            arrayName.push();
*/

console.log("Current array: ");
console.log(fruits);
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log(fruits);

// without return
/*
function addFruit(fruit){
    fruits.push(fruit);
    console.log(fruits);
}
addFruit("Gum gum fruit");
*/

// with return
/*
function addFruit(newFruit){
    fruits.push(newFruit);
    return newFruit;
}
let addedFruit = ("Gum gum fruit");
addFruit(addedFruit);
console.log(fruits);
*/

// pop()
/*
    Removes the last element in an array and returns the removed element
    - Syntax
        arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method: ");
console.log(fruits);

// unshift
/*
    - Adds one or more elements at the beginning of an array
    - Syntax
        arrayName.unshift("elementA");
        arrayName.unshift("elementA", "elementB");
*/
fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method: ");
console.log(fruits);

// shift
/*
    - Removes an element at the beginning of an array and returns the removed element
    - Syntax
        arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift methond: ");
console.log(fruits);

// splice
/*
    - Simultaneously removes elements from a specified index number and adds element or elements
    - Syntax
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
*/

fruits.splice(2, 3, "Coconut", "Tomatoe", "Atis");
console.log("Mutated array from splice method");
console.log(fruits);

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method");
console.log(fruits);

// sort
/*
    - Rearranges the array elements in alphanumberic order
    - Syntax
        arrayName.sort();
*/
fruits.sort();
console.log("Mutated array from sort method: ");
console.log(fruits);

// reverse()

/*
    - Reverses the order of array elements
    - Syntax
        arrayName.reverse();
*/
fruits.reverse();
console.log("Mutated array from reverse method: ");
console.log(fruits);

//Non-Mutator Methods
/*
    - Non-Mutator methods are functions that do not modify or change an array after they're created
    - These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
*/

console.log("--- Non-Mutator Methods ---");

let countries = ["US","PH","CAN","SG","TH","PH","FR","DE"];
// indexOf()
/*
    - Syntax
        arrayName.indexOf(searchValue);
        arrayName.indexOf(searchValue, fromIndex);
*/

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf('PH'): " + firstIndex);

let indexOfSecondPH = countries.indexOf("PH", 2);
console.log("Result of indexOf('PH'): " + indexOfSecondPH);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf('BR'): " + invalidCountry);

/*
This is just an example with prompt

let searchIndex = prompt("Search country: ").toUpperCase();
let foundIndex = countries.indexOf(searchIndex);
console.log("Result of search: " + foundIndex);
*/

// lastIndexOf
/*
    - Returns the index number of the last matching element found in an array;
    - The search process will be done from last element proceeding to the first element
    - Syntax
        arrayName.lastNameOf(searchValue);
        arrayName.lastIndexOf(searchValue, fromIndex);
*/

// Getting the index number starting from the last element

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf() method: " + lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf() method: " + lastIndexStart);


// toString()
let stringArray = countries.toString();
console.log("Result from toString() method: ");
console.log(stringArray);
console.log(countries);

// concat()
/*
    - Combines two arrays and returns the combined result
    - Syntax
        arrayA.concat(arrayB);
        arrayA.concat(elementA);
*/

// Combining 2 arrays only
let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breathe bootstrap"];
let taskArrayC = ["get git", "cook node"];

let task = taskArrayA.concat(taskArrayB);
console.log(task);


// Combining multiple arrays
let allTask = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTask);


// Combining arrays with elements
let combinedTask = taskArrayA.concat("smell express", "have react");
console.log(combinedTask);


// join()
/*
    - Returns an array as a string seperated by specified seperator string
    - Syntax
        arrayName.join("seperator");
*/

let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join(" | "));


// Iteration Methods

// forEach()
/*
    - Syntax
        arrayName.forEach(function(individualElement){
            statement
        })
*/

// allTask = ['drink html', 'eat javascript', 'inhale css', 'breathe bootstrap', 'get git', 'cook node']

allTask.forEach(function(task){
    console.log(task);
});

let filteredTasks = [];
allTask.forEach(function(perElement){
    if(perElement.length > 10){
        filteredTasks.push(perElement);
    }
});
console.log("Result of filteredTask: ");
console.log(filteredTasks);

// map()
/*
    - Iterates on each element AND returns new array with different values depending on the result of the function's operation
    - This is useful for performing tasks where mutating/changing the elements are required
    - Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operatio

    - Syntax
        let/const resultArry = arrayName.map(function(individualElement));
*/
let numbers = [1, 2, 3, 4, 5];
let numbersMap = numbers.map(function(element){
    return element * 10;
});
console.log(numbers);
console.log(numbersMap);

// forEach() loops does not return an array compared to map();
let numberForEach = numbers.forEach(function(element){
    return element * 10;
});
console.log(numberForEach);

// every() same like AND condition
/*
    - Checks if all elements in an array meet the given condition
    - This is useful for validating data stored in arrays especially when dealing with large amounts of data
    - Returns a true value if all elements meet the condition and false if otherwise
    - Syntax
        let/const resultArray = arrayName.every(function(indivElement) {
            return expression/condition;
        })
*/
// numbers = [1, 2, 3, 4, 5];
let allValid = numbers.every(function(element){
    return (element < 3);
});
console.log("Result of every method: ");
console.log(allValid);

// some() same like OR condition
let someValid = numbers.some(function(element){
    return(element < 3);
});
console.log("Result of some method: ");
console.log(someValid);

// filter()
let filterValid = numbers.filter(function(element){
    return(element < 3);
});
console.log(filterValid);

/*
    - Returns new array that contains elements which meets the given condition
    - Returns an empty array if no elements were found
    - Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods
    - Mastery of loops can help us work effectively by reducing the amount of code we use
    - Several array iteration methods may be used to perform the same result
    - Syntax
        let/const resultArray = arrayName.filter(function(indivElement) {
            return expression/condition;
        })
*/

// No elements found
let nothingFound = numbers.filter(function(element){
    return(element == 0);
});
console.log(nothingFound);

// includes()
/*
- includes() method checks if the argument passed can be found in the array.
    - it returns a boolean which can be saved in a variable.
    - returns true if the argument is found in the array.
    - returns false if it is not.
    - Syntax:
        arrayName.includes(<argumentToFind>)
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
console.log(products.includes("Mouse"));

let productFound2 = products.includes("Headset");
console.log(productFound2);

// reduce() 
    /* 
    - Evaluates elements from left to right and returns/reduces the array into a single value
    - Syntax
        let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
            return expression/operation
        })
    - The "accumulator" parameter in the function stores the result for every iteration of the loop
    - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
    - How the "reduce" method works
        1. The first/result element in the array is stored in the "accumulator" parameter
        2. The second/next element in the array is stored in the "currentValue" parameter
        3. An operation is performed on the two elements
        4. The loop repeats step 1-3 until all elements have been worked on
*/

// numbers = [1, 2, 3, 4, 5];
let iteration = 0;
let reducedArray = numbers.reduce(function(acc,cur){
    console.warn("current iteration " + ++iteration);
    console.log("accumulator: " + acc); // 1 // 3 // 6 // 10
    console.log("current value: " + cur); // 2 // 3 // 4 // 5

    return acc + cur; // 3 // 6 // 10 // 15
});
console.log("Result of reduce method");
console.log(reducedArray);







